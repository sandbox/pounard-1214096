<?php

/**
 * @file
 * Installation hooks.
 */

/**
 * Implementation of hook_schema().
 */
function lock_schema() {

  $schema = array();

  $schema['semaphore_lock'] = array(
    'description' => 'Applicative lock',
    'fields' => array(
      'name' => array(
        'description' => 'Lock name.',
        'type'        => 'varchar',
        'length'      => 128,
        'not null'    => true,
        'default'     => ''
      ),
      'value' => array(
        'description' => 'Lock group.',
        'type'        => 'varchar',
        'length'      => 64,
        'not null'    => true,
        'default'     => ''
      ),
      'token' => array(
        'description' => 'A token value that can ensure locking attempt to be done many time on the same object.',
        'type'        => 'varchar',
        'length'      => 128,
        'not null'    => true,
        'default'     => '',
      ),
      'uid' => array(
        'description' => 'The user uid that triggered this lock.',
        'type'        => 'int',
        'not null'    => true,
        'default'     => 0,
      ),
      'expire' => array(
        'description' => 'A Unix timestamp with microseconds indicating when the semaphore should expire.',
        'type'        => 'float',
        'size'        => 'big',
        'not null'    => true
      ),
    ),
    'indexes' => array(
      'expire_idx' => array('expire'),
      'uid_idx'    => array('uid'),
      'value_idx'  => array('value'),
      'token_idx'  => array('token'),
    ),
    'primary key' => array('name', 'value'),
  );

  return $schema;
}
