<?php

namespace Lock;

class MemoryLockBackend implements LockBackendInterface
{
    public function acquire(
        $name,
        $lifetime  = LockBackendInterface::LIFETIME_DEFAULT,
        $group     = LockBackendInterface::DEFAULT_GROUP,
        $accountId = null,
        $token     = null)
    {
        return new Lock($this, $name, $group, $accountId, $token);
    }

    public function release($name, $token = null)
    {
    }

    public function releaseAll($token = null)
    {
    }

    public function getLock($name, $group = null, $accountId = null, $token = null)
    {
    }

    public function wipeOutLocks($accountId = null, $group = null, $token = null)
    {
    }
}
