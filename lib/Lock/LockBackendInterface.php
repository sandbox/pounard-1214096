<?php

namespace Lock;

/**
 * Simple semaphore based persistant locking framework
 *
 * Each acquired lock is named using a unique and predictable name across
 * concurrent requests or sessions: two locks with the same name cannot
 * exist in the backend.
 *
 * Each lock has a lifetime, if you are working on a lock being used
 * for a long amount of time and across multiple HTTP requests, you
 * must try to acquire the lock each time you need to operate on the
 * targeted business object: this will ensure that in case of expiration
 * you won't try to operate on an object locked by someone else.
 *
 * Locks can have numerous other properties:
 *
 *  - accountId : this property only serves the purpose of linking a lock
 *    to its owner.
 *    It does not have any other meaning to the backend outside of being
 *    able to be queried.
 *
 *  - group : this property should be used in order to give a group of
 *    lock a business meaning and group them altogether.
 *    It does not have any other meaning to the backend outside of being
 *    able to be queried.
 *
 *  - token : this property is mandatory in most cases. The lock token is
 *    what identifies the user real session. If you try to acquire the
 *    same lock twice using the same token, the acquire will succeed, if
 *    you try but the given token is different from the existing lock,
 *    it will fail. Ideally it should always be stored into the user
 *    session and/or linked to a current specific user tab (meaning that
 *    a specific business operation being done cannot leak over another
 *    in the same session).
 *
 * A default token should always be set into the backend for making it
 * easier to the user, making the token parameter optional in most cases;
 * Nevertheless for advanced usages (for example a running batch in a
 * specific browser tab) the API using it should probably compute a
 * unique and single token for the pending batch.
 */
interface LockBackendInterface
{
    /**
     * Default lifetime for locks
     */
    const LIFETIME_DEFAULT = 30;

    /**
     * Default group for locks
     */
    const DEFAULT_GROUP = 'default';

    /**
     * Set default account identifier
     *
     * @param int $accountId
     */
    public function setDefaultAccountId($accountId);

    /**
     * Get default account identifier
     *
     * @return int
     */
    public function getDefaultAccountId();

    /**
     * Set default session token
     *
     * @param string $token
     */
    public function setSessionToken($token);

    /**
     * Get default session token
     *
     * @param string $token
     */
    public function getSessionToken();

    /**
     * Acquire a lock
     * 
     * @param string $name
     *   Lock name
     * @param int $lifetime
     *   Lock lifetime (in seconds)
     * @param string $group
     *   A group name for lock, this will allow to remove all locks for
     *   a certain group
     * @param int $accountId
     *   User account identifier, if set, the current lock will be linked
     *   to the current logged in user (except for anonymous). This means
     *   that on session destroy if no other session is active, the lock
     *   will be removed
     * @param string $token
     *   If set, set a token the lock. Tokens will allow more than one
     *   locking attempt on the same lock, using the same token. It can
     *   also allow lock sharing among users. If no token is provided the
     *   default session token will be used
     *
     * @return Lock
     *   Newly acquired lock
     *
     * @throws LockAlreadyAcquiredException
     *   If lock already been acquired by another thread
     */
    public function acquire(
        $name,
        $lifetime  = LockBackendInterface::LIFETIME_DEFAULT,
        $group     = LockBackendInterface::DEFAULT_GROUP,
        $accountId = null,
        $token     = null);

    /**
     * Release a lock
     *
     * @param string $name
     *   Lock name
     * @param string $token
     *   Lock token, if none given will use the session one
     *
     * @throws AlreadyAcquiredException
     *   If lock is owner by another token
     */
    public function release($name, $token = null);

    /**
     * Release a lock
     *
     * @param string $token
     *   Lock token, if none given will use the session one
     *
     * @throws AlreadyAcquiredException
     *   If lock is owner by another token
     */
    public function releaseAll($token = null);

    /**
     * Load an existing lock
     *
     * Important: this method does not acquire the lock
     *
     * @param string $name
     *   Lock name
     *
     * @return Lock
     *   Lock object if exists otherwise false
     */
    public function getLock($name);

    /**
     * Clear outdated locks
     *
     * Both of the following parameters can be used at once
     *
     * @param int $accountId
     *   If set, this will only wipe out all locks for the given
     *   user identifier
     * @param string $group
     *   If set, this will wipe out all locks for the given group
     * @param string $token
     *   If set, this will wipe out all locks for the given token
     */
    public function wipeOutLocks($accountId = null, $group = null, $token = null);
}
