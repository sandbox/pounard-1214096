<?php

namespace Lock;

class MemoryLockBackend implements LockBackendInterface
{
    public function acquire(
        $name,
        $lifetime  = LockBackendInterface::LIFETIME_DEFAULT,
        $group     = LockBackendInterface::DEFAULT_GROUP,
        $accountId = null,
        $token     = null)
    {
        throw new \Exception("Not implemented yet");
    }

    public function release($name, $token = null)
    {
        throw new \Exception("Not implemented yet");
    }

    public function releaseAll($token = null)
    {
        throw new \Exception("Not implemented yet");
    }

    public function getLock($name, $group = null, $accountId = null, $token = null)
    {
        throw new \Exception("Not implemented yet");
    }

    public function wipeOutLocks($accountId = null, $group = null, $token = null)
    {
        throw new \Exception("Not implemented yet");
    }
}
