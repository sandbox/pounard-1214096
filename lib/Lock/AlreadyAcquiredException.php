<?php

namespace Lock;

/**
 * Exception thrown when aptempting to acquire an already locked lock.
 */
class AlreadyAcquiredException extends \RuntimeException
{
    /**
     * @var Lock
     */
    private $lock;

    /**
     * Default constructor
     *
     * @param Lock $lock
     */
    public function __construct(Lock $lock)
    {
        parent::__construct("Cannot acquire lock");

        $this->lock = $lock;
    }

    /**
     * Get the already acquired lock
     *
     * @return Lock
     */
    public function getLock()
    {
        return $this->lock;
    }
}
