<?php

namespace Lock;

/**
 * Default implementation that handles the session token that would fit
 * most use cases
 */
abstract class AbstractLockBackend implements LockBackendInterface
{
    /**
     * Default session key name
     */
    const SESSION_KEY = '_lock_token';

    /**
     * Default account identifier
     *
     * @var int
     */
    private $defaultAccountId = 0;

    /**
     * Default token
     *
     * @var string
     */
    private $sessionToken;

    /**
     * @var callable
     */
    private $defaultTokenGetter;

    public function setDefaultAccountId($accountId)
    {
        $this->defaultAccountId = $accountId;
    }

    public function getDefaultAccountId()
    {
        return $this->defaultAccountId;
    }

    /**
     * Change lazy default token getter
     *
     * Default will lookup into the $_SESSION array
     *
     * @param callback $callback
     *   A callback that takes no parameters and return a string. Note
     *   that it can return null in case the implementation will fallback
     *   to default behavior which is to fetch a $_SESSION parameter
     */
    public function setDefaultTokenGetter($callback)
    {
        if (!is_callable($callback)) {
            throw new \InvalidArgumentException("Given parameter is not callable");
        }

        $this->defaultTokenGetter = $callback;
    }

    public function setSessionToken($token)
    {
        $this->sessionToken = $token;
    }

    public function getSessionToken()
    {
        if (null === $this->sessionToken) {
            if (null !== $this->defaultTokenGetter && is_callable($this->defaultTokenGetter)) {
                $this->sessionToken = call_user_func($this->defaultTokenGetter);
            }
            if (null === $this->sessionToken) {
                if (!empty($_SESSION[self::SESSION_KEY])) {
                    $this->sessionToken = $_SESSION[self::SESSION_KEY];
                } else { // FIXME: Sounds like bad random
                    $this->sessionToken = $_SESSION[self::SESSION_KEY] = md5(uniqid(null, true));
                }
            }
        }

        return $this->sessionToken;
    }
}
