<?php

namespace Lock;

class Lock
{
    /**
     * @var LockBackendInterface
     */
    private $backend;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $group;

    /**
     * @var int
     */
    private $accountId;

    /**
     * @var string
     */
    private $token;

    /**
     * Default constructor
     *
     * @param LockBackendInterface $backend
     *   Lock backend this lock belongs to
     * @param string $name
     *   Lock internal unique name
     * @param string $group
     *   Lock group
     * @param string $accountId
     *   User account identifier this lock belongs to
     * @param string $token
     *   Lock token
     */
    public function __construct(
        LockBackendInterface $backend,
        $name,
        $group     = LockBackendInterface::DEFAULT_GROUP,
        $accountId = false,
        $token     = null)
    {
        $this->backend   = $backend;
        $this->name      = $name;
        $this->group     = $group;
        $this->accountId = $accountId;
        $this->token     = $token;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get group
     *
     * @return string
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * Get account identifier
     *
     * @return int
     */
    public function getAccountId()
    {
        return $this->accountId;
    }

    public function getToken()
    {
        return $this->token;
    }

    public function release()
    {
        $this->backend->release($this->name, $this->token);
    }
}
