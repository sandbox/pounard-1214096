<?php

namespace Lock;

/**
 * Default implementation, and singleton implementation. Fine for most usages.
 *
 * @todo This has been rewriten quickly, needs work.
 */
class DatabaseLockBackend extends AbstractLockBackend
{
    /**
     * @var \DatabaseConnection
     */
    private $db;

    /**
     * Default constructor
     *
     * @param \DatabaseConnection $db
     */
    public function __construct(\DatabaseConnection $db)
    {
        $this->db = $db;
    }

    public function acquire(
        $name,
        $lifetime  = LockBackendInterface::LIFETIME_DEFAULT,
        $group     = LockBackendInterface::DEFAULT_GROUP,
        $accountId = null,
        $token     = null)
    {
        $time = time();
        $tx   = null;

        if (null === $token) {
            $token = $this->getSessionToken();
        }
        if (null === $accountId) {
            $accountId = $this->getDefaultAccountId();
        }

        // Aptempt to find if this semaphore already been locked
        try {
            $tx = $this->db->startTransaction();

            $row = $this
                ->db
                ->query("SELECT * FROM {semaphore_lock} WHERE expire > :time AND name = :name", array(
                    ':time' => $time,
                    ':name' => $name,
                ))
                ->fetch();

            if ($row) {
                if ($token && $row->token === $token) {
                    // Success in case of identical tokens
                    return new Lock($this, $name, $row->value, $row->uid, $token);
                } else {
                    throw new AlreadyAcquiredException(
                        $this->getLock($name)
                    );
                }
            } else {
                // Avoid collisions when an expired semaphore still exists
                // with the same name.
                $this->db->query("DELETE FROM {semaphore_lock} WHERE name = :name", array(':name' => $name));
            }

            $values = array(
                'name'   => $name,
                'expire' => $time + $lifetime,
                'value'  => $group,
                'uid'    => $accountId,
                'token'  => $token,
            );

            $this->db->insert('semaphore_lock')->fields($values)->execute();

            unset($tx); // Explicit commit.

            return new Lock($this, $name, $group, $accountId, $token);

        } catch (\Exception $e) {
            if ($tx) {
                try {
                    $tx->rollback();
                } catch (\Exception $e2) {}
            }
            throw $e;
        }
    }

    public function release($name, $token = null)
    {
        if (null === $token) {
            $token = $this->getSessionToken();
        }

        $this->db->query("DELETE FROM {semaphore_lock} WHERE name = :name AND token = :token", array(
            ':name'  => $name,
            ':token' => $token,
        ));
    }

    public function releaseAll($token = null)
    {
        if (null === $token) {
            $token = $this->getSessionToken();
        }

        $this->db->query("DELETE FROM {semaphore_lock} WHERE token = :token", array(':token' => $token));
    }

    public function getLock($name, $group = null, $accountId = null, $token = null)
    {
        $q = $this->db->select('semaphore_lock', 's');
        $q->fields('s');
        $q->condition('s.expire', time(), '>');
        $q->condition('s.name', $name);

        if ($group) {
            $q->condition('s.value', $group);
        }
        if ($accountId) {
            $q->condition('s.uid', $accountId);
        }
        if ($token) {
            $q->condition('s.token', $token);
        }

        $q->orderBy('s.name');
        $q->range(0, 1);

        $values = $q->execute()->fetch();

        return new Lock(
            $this,
            $values->name,
            $values->value,
            $values->uid,
            $values->token
        );
    }

    public function wipeOutLocks($accountId = null, $group = null, $token = null)
    {
        $q = $this->db->delete('semaphore_lock');

        if ($group) {
            $q->condition('value', $group);
        }
        if ($accountId) {
            $q->condition('uid', $accountId);
        }
        if ($token) {
            $q->condition('token', $token);
        }

        if (!$accountId || $group || $token) {
            $q->execute();
        } else {
            $q->condition('expire', time(), '<');
            $q->execute();
        }
    }
}
